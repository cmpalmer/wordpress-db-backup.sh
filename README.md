License
-------

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
[Copying][] for more details.

   [copying]: http://sam.zoy.org/wtfpl/COPYING 

Introduction
------------

A script to back up your wordpress database locally.

Designed to be run from cron or called from a backup tool like [rsnapshot][].
Note that if you're not shipping these backups offsite or at least off-chassis,
you could well end up in the unfortunate position of the author of this script.
Which is to say, you may lose 4+ years of blog posts and comments.

   [rsnapshot]: http://rsnapshot.org/

Dependencies
------------

grep, awk, mysqldump

Assumptions
-----------

Running on the same host with your wordpress directory.

