#!/bin/bash
#
# wordpress-db-backup.sh
# (c) 2012 Cristóbal Palmer cmp@cmpalmer.org
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.
#
# A script to back up your wordpress database locally.
# Designed to be run from cron or called from a backup tool like rsnapshot.
# Note that if you're not shipping these backups offsite or at least off-chassis,
# you could well end up in the unfortunate position of the author of this script.
# Which is to say, you may lose 4+ years of blog posts and comments.
#
# dependencies: grep, awk, mysqldump
# assumptions: running on the same host with your wordpress directory

# set the wordpress directory
WPDIR=""
# set the backups directory
BACKUPSDIR=""

# no need to change below this line

pushd $WPDIR || exit 1
if [ -f "wp-config.php" ]; then
  for i in $(grep "^define.*DB_" wp-config.php | awk -F\' '{print $2"="$4}'); do
    #echo $i
    export $i
  done
else
  echo "wp-config.php not found, exiting"
  exit 1
fi
popd

pushd $BACKUPSDIR || exit 1
# a file to back up to is nice
BACKUPSFILE=$DB_NAME.$(date +%F_%H-%M).$RANDOM.sql
touch $BACKUPSFILE || exit 1
mysqldump -u $DB_USER -h $DB_HOST -p$DB_PASSWORD --ignore-table=wordpress.wp_bad_behavior $DB_NAME > $BACKUPSFILE
if [ -s "$BACKUPSFILE" ]; then
  gzip $BACKUPSFILE || exit 1
else
  rm $BACKUPSFILE
  echo "Something bad happened with the mysqldump, exiting."
  exit 1
fi
popd

unset $DB_USER $DB_HOST $DB_PASSWORD $DB_NAME
exit 0

